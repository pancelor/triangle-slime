local dev
-- dev=true

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local confusion = require "necro.game.character.Confusion"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

--[[
you need to also install https://gitlab.com/pancelor/pancelor-enemy-pools
  for this mod to work
]]
local enemyPool = require "enemypool.EnemyPool"

local RNG_CHANNEL=1284

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    object.spawn("triangleslime_slime",-1,-1)
  end)
end

components.register {
  marker={
    components.field.int32("kind",0),
  }
}

--[[
note: you can do something similar by instead setting
  aiPattern={moves={action.Direction.UP,action.Direction.RIGHT,action.Direction.DOWN_LEFT}}
on the custom slime entity
]]
event.preAI.add("doTriangleSlime", {order="specialAction", filter="triangleslime_marker"}, function(ev)
  local count=ev.entity.beatDelay.counter
  local kind=ev.entity.triangleslime_marker.kind
  if kind==0 then
    if count==0 then
      ev.action=action.Direction.UP
    elseif count==4 then
      ev.action=action.Direction.RIGHT
    elseif count==2 then
      ev.action=action.Direction.DOWN_LEFT
    else
      ev.action=action.Direction.NONE
    end
  else
    if count==0 then
      ev.action=action.Direction.DOWN
    elseif count==4 then
      ev.action=action.Direction.UP_LEFT
    elseif count==2 then
      ev.action=action.Direction.RIGHT
    else
      ev.action=action.Direction.NONE
    end
  end
end)

--[[
don't use this "replaceWithTriangleSlime" function! this code _does_ work, but
  it's preferred to replace enemies using the pancelor-enemy-pools mod instead.
why? it allows multiple mods to replace the same enemies in a way that's fair to
  all mods (i.e. you can prevent a single third-party mod from replacing _all_
  slimes and leaving none left for your mod to replace) and prevents finicky
  seeding issues (i.e. needing two racers to have their enemy mods activated
  in the exact same order as each other)
]]
-- local function replaceWithTriangleSlime(name,chance)
--   local old=ecs.getEntitiesByType(name)
--   while old:next() do
--     if rng.roll(chance,RNG_CHANNEL) then
--       object.spawn("triangleslime_slime",old.position.x,old.position.y)
--       object.delete(old)
--     end
--   end
-- end

event.objectSpawn.add("modifyTriangleSlime", {order="attributes", sequence=1, filter="triangleslime_marker"}, function(ev)
  local kind=rng.int(2,RNG_CHANNEL)
  ev.entity.triangleslime_marker.kind=kind
end)

event.levelLoad.add("registerTriangleSlime", {order="entities", sequence=1}, function(ev)
  enemyPool.registerConversion{from="Slime2",to="triangleslime_slime",probability=0.2}
  enemyPool.registerConversion{from="Slime3",to="triangleslime_slime",probability=0.2}
  -- replaceWithTriangleSlime("Slime2", 0.2)
  -- replaceWithTriangleSlime("Slime3", 0.2)
end)

--[[
run `tail -f Synchrony.log` to see the output of `dbg`.
]]
-- event.entitySchemaLoadNamedEntity.add("debug", {key="Slime4"}, function (ev)
--   dbg(ev.entity)
-- end)

customEntities.extend {
  name="slime",
  template=customEntities.template.enemy("slime",2),
  components={
    friendlyName={name="Triangle Slime"},
    triangleslime_marker={},
    health={health=3, maxHealth=3},
    dropCurrencyOnDeath={amount=3},
    beatDelay={interval=6},
    dig={
      silentFail=true,
      strength=1,
    },
    sprite={
      texture="mods/triangleslime/images/triangle-slime.png",
    },
  },
}
